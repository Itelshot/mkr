<?php

include 'Bugaltery.php';
class BugTest extends PHPUnit\Framework\TestCase
{
    private $bugaltery;

    protected function setUp(): void
    {
        $this->bugaltery = new Bugaltery();
    }

    public function test_SingerFIOTest(){
        $this->assertEquals(1040, $this->bugaltery->getTotalBalance());
    }

    /**
     * @dataProvider addDataProvider
     */
    public function testSingerRating($type, $expected){

        $this->assertEquals($expected, $this->bugaltery->getCountByType($type));
    }

    public function addDataProvider(){
        return [
            ['debit', 3],
            ['credit', 2],
        ];
    }


    protected function tearDown(): void
    {
        unset($this->eurovission);
    }
}