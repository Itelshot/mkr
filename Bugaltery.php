<?php
include 'BankCard.php';
class Bugaltery
{
    private $cards;

    public function __construct()
    {
        $this->cards = array(
          new BankCard('4141','240','debit'),
          new BankCard('4141','100','credit'),
          new BankCard('4848','300','debit'),
          new BankCard('4141','400','debit'),
        );
    }

    public function getTotalBalance(){
        $totalbalance = 0;
        foreach ($this->cards as $card) {
            $totalbalance += $card->getBalance();
        }
        return $totalbalance;
    }

    public function getCountByType($type){
        $count = 0;
        foreach ($this->cards as $card) {
            if($card->getType() == $type)
                $count++;
        }
        return $count;
    }

}