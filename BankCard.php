<?php


class BankCard
{
private $number;
private $balance;
private $type;

    /**
     * BankCard constructor.
     * @param $number
     * @param $balance
     * @param $type
     */
    public function __construct($number, $balance, $type)
    {
        $this->number = $number;
        $this->balance = $balance;
        $this->type = $type;
    }


    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}